
/* 
	Problem 1: Write a function that will return a particular board's information based on the boardID that is passed from the given list of 
    boards in boards.json and then pass control back to the code that called it by using a callback function.
*/

function boardsInformation( board, boardID, cb) {
   setTimeout( () =>{
    let getdata = board.filter((data) =>{
    return data.id == boardID;
 });
 cb(getdata);
},3*1000);
    
};
function callback(data) {
    console.log(data);
};

module.exports = { boardsInformation, callback }