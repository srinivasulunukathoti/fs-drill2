
/* 
	Problem 5: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind and Space lists simultaneously
*/


function getInformationBoardsListsCardsInMindAndSpace(board ,list , cards, boardsInformation, listInformation, cardsInformation) {
    boardsInformation.boardsInformation(board , "mcu453ed",(boardThanos) => {
     console.log(boardThanos);
     listInformation.listInformation(list , 'mcu453ed', (thanosList) =>{
         console.log(thanosList);
         thanosList.forEach((element )=> {
            if (element.name === "Mind" || element.name === "Space") {
                cardsInformation.cardsInformation(cards ,element.id ,(mindCardsAndCords) => {
                    console.log(mindCardsAndCords);
                })
            }
        });
     })
    })
 };
 module.exports = getInformationBoardsListsCardsInMindAndSpace;