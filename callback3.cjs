
/* 
	Problem 3: Write a function that will return all cards that belong to a particular list based on the listID that is passed to it from the given data in cards.json. Then pass control back to the code that called it by using a callback function.
*/

function cardsInformation(cards , listID , cb) {
   setTimeout(()=>{
    let getCardsDeatails={};
    for (const key in cards) {
       if (key == listID) {
        getCardsDeatails=cards[key];
       }
    };
    cb(getCardsDeatails);
   },3*1000);
}
function callback(data) {
    console.log(data);
};

// cartsInformation(cards , listID , callback);
module.exports = { cardsInformation , callback };