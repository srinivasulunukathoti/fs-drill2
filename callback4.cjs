/* 
	Problem 4: Write a function that will use the previously written functions to get the following information.
     You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind list simultaneously
*/


function getInformationBoardsListsCards(board ,list , cards, boardsInformation, listInformation, cardsInformation) {
   boardsInformation.boardsInformation(board , "mcu453ed",(boardThanos) => {
    console.log(boardThanos);
    listInformation.listInformation(list , 'mcu453ed', (thanosList) =>{
        console.log(thanosList);
        cardsInformation.cardsInformation(cards , 'qwsa221',(mindCards) => {
            console.log(mindCards);
        })
    })
   })
};
// getInformationBoardsListsCards(board , list , cards);
module.exports = getInformationBoardsListsCards;